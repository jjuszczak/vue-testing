build-lists: true
slide-transition: true
slidenumbers: true

# *Vue.js*
## Unit Tests

---

# Overview

---

[.column]

# Technical

- Testing props
- Testing methods
- Snapshot tests
- Stubbing components

[.column]

# Design

- Defining test scenarios
- Regression

---

# Why (unit) *test* at all?

---

# Advantages of unit tests

- Raise quality
- Simplify debug process
- Easier and safer code refactoring
- Reducing bug fixing cost

--- 

#[fit] Testing components
## [fit]with *jest* and *vue-test-utils*

--- 

# Testing *props*

---

```html
<template>
  <div class="hello">
    <h1>{{ msg }}</h1>
  </div>
</template>

<script>
export default {
  name: "HelloWorld",
  props: {
    msg: String,
  },
};
</script>

```

---


```js
import { shallowMount } from "@vue/test-utils";
import HelloWorld from "@/components/HelloWorld.vue";

describe("HelloWorld.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });
});

```

---

# Testing *interactions*

---

```html
<template>
  <div>
    Count is <span class="count">{{ count }}</span>
    <button @click="increment" class="btn">Increase</button>
  </div>
</template>

<script>
export default {
  name: "MethodTest",
  data: () => ({
    count: 0,
  }),
  methods: {
    increment() {
      this.count++;
    },
  },
};
</script>

```

---

```js
import { shallowMount } from "@vue/test-utils";
import Interaction from "@/components/Interaction.vue";

describe("Interaction.vue", () => {
  it("counter should increment on button click", async () => {
    const wrapper = shallowMount(Interaction);

    await wrapper.find(".btn").trigger("click");
    expect(wrapper.find(".count").text()).toContain(1);
    expect(wrapper.vm.count).toEqual(1);
  });
});

```

---

# Testing *events*

---

```html
<template>
  <button class="btn" @click="start">Click me! 🔮</button>
</template>

<script>
export default {
  name: "Event",
  methods: {
    start() {
      this.$emit("start");
    },
  },
};
</script>

```

---


```js
import { shallowMount } from "@vue/test-utils";
import Event from "@/components/Event.vue";

describe("Event.vue", () => {
  it("should emit start event on button click", async () => {
    const wrapper = shallowMount(Event);
    await wrapper.find(".btn").trigger("click");
    expect(wrapper.emitted("start")).toBeTruthy();
  });
});


```

---

# Testing *functions*

---

```html
<template>
  <button class="btn" @click="clickFn">Click me! 🔮</button>
</template>

<script>
export default {
  name: "FunctionProp",
  props: {
    clickFn: {
      type: Function,
      default: () => ({}),
    },
  },
};
</script>

```

---

```js
import { shallowMount } from "@vue/test-utils";
import FunctionProp from "@/components/FunctionProp.vue";

describe("FunctionProp.vue", () => {
  it("counter should increment on button click", async () => {
    const clickFunction = {
      clickIt() {
        return true;
      },
    };

    const spy = jest.spyOn(clickFunction, "clickIt");
    const wrapper = shallowMount(FunctionProp, {
      propsData: {
        clickFn: clickFunction.clickIt,
      },
    });
    
    await wrapper.find(".btn").trigger("click");

    expect(spy).toHaveBeenCalled();
  });
});


```

---


```js
it("should increment counter with jest.fn", async () => {
    const clickFn = jest.fn();
    const wrapper = shallowMount(FunctionProp, {
      propsData: {
        clickFn,
      },
    });
    await wrapper.find(".btn").trigger("click");
    expect(clickFn).toHaveBeenCalled();
  });
```

---

# Testing *methods*

---

```html
<template>
  <button class="btn" @click="start">Click me! 🔮</button>
</template>

<script>
export default {
  name: "Method",
  methods: {
    start() {
      console.log("Make request or trigger vuex action");
    },
  },
};
</script>

```

---

```js
import { shallowMount } from "@vue/test-utils";
import MethodComponent from "@/components/Method.vue";

describe("Method.vue", () => {
  it("start Method should be called on button click", async () => {
    const spy = jest.spyOn(MethodComponent.methods, "start");
    const wrapper = shallowMount(MethodComponent);
    await wrapper.find(".btn").trigger("click");
    expect(spy).toHaveBeenCalled();
  });
});

```

---

```html
<template>
  <button class="btn" @click="start">Click me! 🔮</button>
</template>

<script>
export default {
  name: "Method",
  methods: {
    start() {
      this.play();
      this.play();
    },
    play() {
      return false;
    },
  },
};
</script>

```

---

```js
import { shallowMount } from "@vue/test-utils";
import MethodTwice from "@/components/MethodTwice.vue";

describe("MethodTwice.vue", () => {
  it("'start' method should be called once and pause twice on button click", async () => {
    const spyStart = jest.spyOn(MethodTwice.methods, "start");
    const spyPlay = jest.spyOn(MethodTwice.methods, "play");

    const wrapper = shallowMount(MethodTwice);
    await wrapper.find(".btn").trigger("click");
    expect(spyStart).toHaveBeenCalled();
    expect(spyPlay).toHaveBeenCalledTimes(2);
  });
});


```


---

# **Snapshot** testing

---

# Snapshot testing

- Creates a local "snapshot" of the DOM
- On test run, renders the component and compares to snapshot

---

```html
<template>
  <div :class="{ 'is-authenticated': authenticated }">
    <h1 v-if="authenticated">Hello User!</h1>
    <h2 v-else>Good day Guest!</h2>
  </div>
</template>

<script>
export default {
  name: "Snapshot",
  props: {
    authenticated: {
      type: Boolean,
      default: false,
    },
  },
};
</script>

```

---

```js
import { shallowMount } from "@vue/test-utils";
import Snapshot from "@/components/Snapshot.vue";

describe("Snapshot.vue", () => {
  it("should greet guest if not authenticated", () => {
    const wrapper = shallowMount(Snapshot);
    expect(wrapper.element).toMatchSnapshot();
  });

  it("should greet user if authenticated", () => {
    const wrapper = shallowMount(Snapshot, {
      propsData: {
        authenticated: true,
      },
    });
    expect(wrapper.element).toMatchSnapshot();
  });
});


```

---

```html
// Jest Snapshot v1, https://goo.gl/fbAQLP

exports[`Snapshot.vue should greet guest if not authenticated 1`] = `
<div
  class=""
>
  <h2>
    Good day Guest!
  </h2>
</div>
`;

exports[`Snapshot.vue should greet user if authenticated 1`] = `
<div
  class="is-authenticated"
>
  <h1>
    Hello User!
  </h1>
</div>
`;

```

---

# *Stubbing* components

---

# Parent component

```html
<template>
  <div>
    <AsyncComponent />
  </div>
</template>

<script>
import AsyncComponent from "./AsyncComponent";
export default {
  name: "ParentComponent",
  components: { AsyncComponent },
};
</script>

```

---

# Async component
```html
<template>
  <div>Rendering data</div>
</template>

<script>
import axios from "axios"
export default {
  name: "AsyncComponent",
  created() {
    this.makeApiCall();
  },
  methods: {
    async makeApiCall() {
      console.log("Making api call");
      await axios.get('/api/v2/documents')
    },
  },
};
</script>

```

---

# Unit test

```js
import { mount } from "@vue/test-utils";
import ParentComponent from "@/components/Stub/ParentComponent.vue";
import AsyncComponent from "@/components/Stub/AsyncComponent.vue";

describe("ParentComponent.vue", () => {
  it("should mount async component and init api call", () => {
    const wrapper = mount(ParentComponent);
    expect(wrapper.findComponent(AsyncComponent).exists()).toBe(true);
  });
});

```
---

This would trigger the API call.

![inline](./api.png)

---

```js
  it("should mount async component with stub", () => {
    const wrapper = mount(ParentComponent, {
      stubs: {
        AsyncComponent: true,
      },
    });
    expect(wrapper.findComponent(AsyncComponent).exists()).toBe(true);
  });

```
---

```js
 it("should mount async component with automatic stub", () => {
    const wrapper = shallowMount(ParentComponent);
    expect(wrapper.findComponent(AsyncComponent).exists()).toBe(true);
  });
```


---

# Conclusion

- stubs is useful for stubbing out the behavior of children that is unrelated to the current unit test
- shallowMount stubs out child components by default
- you can pass true to create a default stub, or pass your own custom implementation


---
# 👨‍🔬 Designing Test Cases


- Let it fail!
- Which behaviour do you want to test?
- Test your code, not others.
- Don't be afraid to refactor components to be better testable


---


# 👨‍🔬 Designing Test Cases

- What could go wrong?
- Don't only test the valid or right way.
- Test also wrong paths & unexpected inputs



---

```js
const allowedExtensions = ["png"];

export const isAllowedFileExtension = (filename) => {
  const extension = filename.split(".")[1];
  return allowedExtensions.includes(extension);
};

```

---

```js
import { isAllowedFileExtension } from "@/utils/utils.js";
describe("utils.js", () => {
  it("should only allow png files", () => {
    expect(isAllowedFileExtension("hello.png")).toBe(true);
    expect(isAllowedFileExtension("hello.jpg")).toBe(false);
    expect(isAllowedFileExtension("hello.exe")).toBe(false);
    expect(isAllowedFileExtension("hello.php")).toBe(false);
  });
});

```

---



# **Hmm**

---

# What about?

```js
isAllowedFileExtension("file.PNG")
```

---

![inline](./test1.png)

---

# What about ?

```js
isAllowedFileExtension("hello.png.php");
```

---

![inline](./test2.png)


---

# Regression

---

# Fixed a *Bug*? 🐛
## Write a *test*!

---

# References

https://jestjs.io/docs/jest-object

https://lmiller1990.github.io/vue-testing-handbook

https://testdriven.io/blog/vue-unit-testing/

https://apollo.vuejs.org/guide/testing.html#tests-with-mocked-graqhql-schema

---

# Git

https://gitlab.com/jjuszczak/vue-testing

---


#  🙏Thanks for your *attention*!

