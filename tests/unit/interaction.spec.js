import { shallowMount } from "@vue/test-utils";
import Interaction from "@/components/Interaction.vue";

describe("Interaction.vue", () => {
  it("counter should increment on button click", async () => {
    const wrapper = shallowMount(Interaction);

    await wrapper.find(".btn").trigger("click");
    expect(wrapper.find(".count").text()).toContain(1);
    expect(wrapper.vm.count).toEqual(1);
  });
});
