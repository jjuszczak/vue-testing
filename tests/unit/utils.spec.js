import { isAllowedFileExtension } from "@/utils/utils.js";
describe("utils.js", () => {
  it("should only allow png files", () => {
    expect(isAllowedFileExtension("hello.png")).toBe(true);
    expect(isAllowedFileExtension("hello.jpg")).toBe(false);
    expect(isAllowedFileExtension("hello.exe")).toBe(false);
    expect(isAllowedFileExtension("hello.php")).toBe(false);
    // expect(isAllowedFileExtension("hello.PNG")).toBe(true);
    // expect(isAllowedFileExtension("hello.png.php")).toBe(false);
  });
});
