import { shallowMount } from "@vue/test-utils";
import MethodComponent from "@/components/Method.vue";

describe("Method.vue", () => {
  it("start Method should be called on button click", async () => {
    const spy = jest.spyOn(MethodComponent.methods, "start");
    const wrapper = shallowMount(MethodComponent);
    await wrapper.find(".btn").trigger("click");
    expect(spy).toHaveBeenCalled();
  });
});
