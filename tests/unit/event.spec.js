import { shallowMount } from "@vue/test-utils";
import Event from "@/components/Event.vue";

describe("Event.vue", () => {
  it("start event should be emitted on button click", async () => {
    const wrapper = shallowMount(Event);
    await wrapper.find(".btn").trigger("click");
    expect(wrapper.emitted("start")).toBeTruthy();
  });
});
