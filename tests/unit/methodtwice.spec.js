import { shallowMount } from "@vue/test-utils";
import MethodTwice from "@/components/MethodTwice.vue";

describe("Method.vue", () => {
  it("start Method should be called on button click", async () => {
    const spyStart = jest.spyOn(MethodTwice.methods, "start");
    const spyPlay = jest.spyOn(MethodTwice.methods, "play");

    const wrapper = shallowMount(MethodTwice);
    await wrapper.find(".btn").trigger("click");
    expect(spyStart).toHaveBeenCalled();
    expect(spyPlay).toHaveBeenCalledTimes(2);
  });
});
