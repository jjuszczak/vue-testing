import { shallowMount } from "@vue/test-utils";
import FunctionProp from "@/components/FunctionProp.vue";

describe("FunctionProp.vue", () => {
  it("counter should increment on button click", async () => {
    const clickFunction = {
      clickIt() {
        return true;
      },
    };

    const spy = jest.spyOn(clickFunction, "clickIt");
    const wrapper = shallowMount(FunctionProp, {
      propsData: {
        clickFn: clickFunction.clickIt,
      },
    });
    await wrapper.find(".btn").trigger("click");

    expect(spy).toHaveBeenCalled();
  });

  it("should increment counter with jest.fn", async () => {
    const clickFn = jest.fn();
    const wrapper = shallowMount(FunctionProp, {
      propsData: {
        clickFn,
      },
    });
    await wrapper.find(".btn").trigger("click");
    expect(clickFn).toHaveBeenCalled();
  });
});
