import { shallowMount } from "@vue/test-utils";
import Snapshot from "@/components/Snapshot.vue";

describe("Snapshot.vue", () => {
  it("should greet guest if not authenticated", () => {
    const wrapper = shallowMount(Snapshot);
    expect(wrapper.element).toMatchSnapshot();
  });

  it("should greet user if authenticated", () => {
    const wrapper = shallowMount(Snapshot, {
      propsData: {
        authenticated: true,
      },
    });
    expect(wrapper.element).toMatchSnapshot();
  });
});
