import { mount, shallowMount } from "@vue/test-utils";
import ParentComponent from "@/components/Stub/ParentComponent.vue";
import AsyncComponent from "@/components/Stub/AsyncComponent.vue";

describe("ParentComponent.vue", () => {
  it("should mount async component and init api call", () => {
    const wrapper = mount(ParentComponent);
    expect(wrapper.findComponent(AsyncComponent).exists()).toBe(true);
  });

  it("should mount async component with stub", () => {
    const wrapper = mount(ParentComponent, {
      stubs: {
        AsyncComponent: true,
      },
    });
    expect(wrapper.findComponent(AsyncComponent).exists()).toBe(true);
  });

  it("should mount async component with automatic stub", () => {
    const wrapper = shallowMount(ParentComponent);
    expect(wrapper.findComponent(AsyncComponent).exists()).toBe(true);
  });
});
