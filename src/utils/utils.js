const allowedExtensions = ["png"];

export const isAllowedFileExtension = (filename) => {
  const extension = filename.split(".")[1];
  return allowedExtensions.includes(extension);
};
